**API DOCUMENT**

This document provides required API documentation in order to know the methods provided in this API:

`/api/subsriptions`: It retrieves all the services registered. 

`/api/subscriptions?name={name}`: It retrieves the service registered with the name specified.

There are 3 services created:
    
- Netflix.
- HBO.
- Spotify.
    
The names of these services are case insensitive.


**EXAMPLES**

http://localhost:8762/api/subscriptions

http://localhost:8762/api/subscriptions?name=hbo


There are others methods but they are used by `uxs-users` microservice.

*Please, in order to make requests to this endpoints, check if zuul server is running.