CREATE TABLE SUBSCRIPTIONS (
	id int not null primary key,
	uuid varchar(30),
	name varchar(30),
	description varchar(255),
	price double,
	uuid_User varchar(30)
);

CREATE SEQUENCE SUBSCRIPTIONS_SEQ start with 1 increment by 1;
