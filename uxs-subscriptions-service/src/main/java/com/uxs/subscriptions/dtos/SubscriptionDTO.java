package com.uxs.subscriptions.dtos;

public class SubscriptionDTO {

	private Long id;
	private String uuid;
	private String description;
	private Double price;

	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public Double getPrice() {
		return price;
	}

	public String getUuid() {
		return uuid;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
