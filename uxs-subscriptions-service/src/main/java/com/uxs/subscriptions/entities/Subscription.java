package com.uxs.subscriptions.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "SUBSCRIPTIONS")
public class Subscription {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, updatable = false)
	private Long id;

	@Column
	@NotNull
	private String uuid;

	@Column
	@NotNull
	private String name;

	@Column
	private String description;

	@Column
	private Double price;

	@Column
	private String uuidUser;

	public String getDescription() {
		return description;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Double getPrice() {
		return price;
	}

	public String getUuid() {
		return uuid;
	}

	public String getUuidUser() {
		return uuidUser;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public void setUuidUser(String uuidUser) {
		this.uuidUser = uuidUser;
	}

}
