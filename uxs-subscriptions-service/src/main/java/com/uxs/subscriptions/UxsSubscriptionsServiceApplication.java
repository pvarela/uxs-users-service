package com.uxs.subscriptions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class UxsSubscriptionsServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UxsSubscriptionsServiceApplication.class, args);
	}

}
