package com.uxs.subscriptions.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uxs.subscriptions.dtos.SubscriptionDTO;
import com.uxs.subscriptions.services.SubscriptionService;

@RestController
@RequestMapping(value = "/subscriptions")
public class SubscriptionController {

	@Autowired
	private SubscriptionService subscriptionService;

	/**
	 * Retrieves all the subscriptions registered in the DB
	 *
	 * @return a list of subscriptions with: id, uuid, name and price
	 */
	@GetMapping()
	public List<SubscriptionDTO> getAll() {
		return subscriptionService.getAll();
	}

	/**
	 * Retrieves the services with the requested name
	 *
	 * @param name service name we want to get
	 * @return a list of the subscriptions with the name specified
	 */
	@GetMapping(params = { "name" })
	public List<SubscriptionDTO> getByName(@RequestParam(name = "name") String name) {
		return subscriptionService.getByName(name);
	}

	/**
	 * Retrieves all the subscribed services with the name specified by the user
	 * with the uuid requested
	 *
	 * @param uuidUser user identification we want to get their services
	 * @param name     name of the service
	 * @return a list of subscriptions
	 */
	@GetMapping(value = "/{uuidUser}/services", params = { "name" })
	public List<SubscriptionDTO> getByNameAndUserUuid(@PathVariable String uuidUser,
			@RequestParam(name = "name") String name) {
		return subscriptionService.getAllByNameAndUserUuid(name, uuidUser);
	}

	/**
	 * Retrieves all the subscribed services by the user with the uuid specified
	 *
	 * @param uuidUser uuidUser user identification we want to get their services
	 * @return the list of subscriptions
	 */
	@GetMapping(value = "/{uuidUser}")
	public List<SubscriptionDTO> getByUserUuid(@PathVariable String uuidUser) {
		return subscriptionService.getByUserUuid(uuidUser);
	}

}
