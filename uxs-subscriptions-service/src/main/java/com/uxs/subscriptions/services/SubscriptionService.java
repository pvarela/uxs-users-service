package com.uxs.subscriptions.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uxs.subscriptions.daos.SubscriptionDAO;
import com.uxs.subscriptions.dtos.SubscriptionDTO;
import com.uxs.subscriptions.entities.Subscription;

import ma.glasnost.orika.MapperFacade;

@Service
public class SubscriptionService {

	@Autowired
	private MapperFacade orika;

	@Autowired
	private SubscriptionDAO subscriptionDao;

	/**
	 * Retrieves all the services registered in the DB
	 * 
	 * @return a list with all the services
	 */
	public List<SubscriptionDTO> getAll() {
		return orika.mapAsList(subscriptionDao.findAll(), SubscriptionDTO.class);
	}

	/**
	 * Retrieves all the services with the name specified which an user is
	 * subscribed to
	 * 
	 * @param name     service name
	 * @param uuidUser user identification
	 * @return
	 */
	public List<SubscriptionDTO> getAllByNameAndUserUuid(String name, String uuidUser) {
		return orika.mapAsList(subscriptionDao.findAllByNameIgnoreCaseAndUuidUser(name, uuidUser),
				SubscriptionDTO.class);
	}

	/**
	 * Retrieves all the services which have the name specified
	 * 
	 * @param name service name
	 * @return a list with the services named as requested
	 */
	public List<SubscriptionDTO> getByName(String name) {
		return orika.mapAsList(subscriptionDao.findAllByNameIgnoreCase(name), SubscriptionDTO.class);
	}

	/**
	 *
	 * @param uuid
	 * @return
	 */
	public List<SubscriptionDTO> getByUserUuid(String uuid) {
		List<Subscription> subscriptions = subscriptionDao.findAllByUuidUser(uuid);
		return orika.mapAsList(subscriptions, SubscriptionDTO.class);
	}

}
