package com.uxs.subscriptions.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uxs.subscriptions.entities.Subscription;

public interface SubscriptionDAO extends JpaRepository<Subscription, Long> {

	/**
	 * Find all the services by name without considering case
	 * 
	 * @param name name of service
	 * @return a list of subscriptions which named services in
	 */
	public List<Subscription> findAllByNameIgnoreCase(String name);

	/**
	 * Find all the services by name without considering case which an user is
	 * subscribed to
	 * 
	 * @param name name of the service
	 * @param uuid user identification
	 * @return a list of subscriptions which the requested user is subscribed to
	 */
	public List<Subscription> findAllByNameIgnoreCaseAndUuidUser(String name, String uuid);

	/**
	 * Find all the services which an user is subscribed to
	 * 
	 * @param uuid user identification
	 * @return a list of subscriptions which the specified user is subscribed to
	 */
	public List<Subscription> findAllByUuidUser(String uuid);

}
