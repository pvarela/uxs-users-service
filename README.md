# uxs-users-service

This repository contains the following projects:

- uxs-users. Microservice that manages all related to users. It communicates, also, with uxs-subscriptions-service in order to provide some information about the users which manages.
- uxs-subscriptions-service. Microservice that manages all related to subscriptions.
- uxs-zuul-server: It's the API Gateway. (Server-side discovery) It includes Ribbon (software load balancer between the multiple microservices instances).
- uxs-eureka-server: It's the Service Registry (Client-side registry) which is responsible for registering all microservices instances (uxs-users, uxs-subscriptions-service and uxs-zuul-server).


All these projects have been developed with the technology described below:

- Java 11.
- Spring Framework. More details in README from every single project.
- Maven 3.5.3 (Embedded in IDE Eclipse 2019-03)
- Orika.
- Liquibase.
- H2 database. To access to the console: http://localhost:{server.port}/h2-console
- "Netflix microservice framework", included in Spring Boot dependencies:
    - spring-cloud-starter-netflix-eureka-server
    - spring-cloud-starter-netflix-eureka-client
    - spring-cloud-starter-netflix-hystrix


These projects are runned using Boot Dashboard which is included in the plugin STS for Eclipse. The method to run these projects are described below:

1.  Run uxs-eureka-server. It's needed because microservices are responsible to connect to eureka in order to be registered. An error will be throw if eureka is not initialized properly. Eureka will be accesible in: http://localhost:8761
2.  Run uxs-zuul-server or the microservices. It will run in port 8762.
3.  Run uxs-users. In order to run multiple instances from this microservice, we could add -Dserver.port=8300 in VM Arguments in Run Configurations.
4.  Run uxs-subscriptions-service. In order to run multiple instances from this microservice, we could add -Dserver.port=8301 in VM Arguments in Run Configurations.
