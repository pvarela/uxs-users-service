CREATE TABLE USERS (
	id int not null primary key,
	uuid varchar(30),
	name varchar(20),
	surname varchar(50),
	age int(2),
	dni varchar(9)
);

CREATE SEQUENCE USERS_SEQ start with 1 increment by 1;
