package com.uxs.users.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uxs.users.daos.UserDAO;
import com.uxs.users.dtos.UserDTO;
import com.uxs.users.entities.User;

import ma.glasnost.orika.MapperFacade;

@Service
public class UserService {

	@Autowired
	private MapperFacade orika;

	@Autowired
	private UserDAO userDao;

	/**
	 * Retrieves all the users registered in the DB
	 * 
	 * @return a list with all users registered
	 */
	public List<UserDTO> getAll() {
		return orika.mapAsList(userDao.findAll(), UserDTO.class);
	}

	/**
	 * Retrieves the user who has the id specified or a default user in other case
	 * 
	 * @param id user identification
	 * @return
	 */
	public UserDTO getById(Long id) {
		User user = userDao.findById(id).orElse(new User());
		return orika.map(user, UserDTO.class);
	}

}
