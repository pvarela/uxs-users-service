package com.uxs.users.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "USERS")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, updatable = false)
	private Long id;

	@Column
	@NotNull
	private String uuid;

	@Column
	@NotNull
	private String name;

	@Column
	private String surname;

	@Column
	private Integer age;

	@Column
	@NotNull
	private String dni;

	public Integer getAge() {
		return age;
	}

	public String getDni() {
		return dni;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getUuid() {
		return uuid;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
