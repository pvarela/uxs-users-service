package com.uxs.users.dtos;

public class UserDTO {

	private Long id;
	private String uuid;
	private String name;
	private String surname;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getUuid() {
		return uuid;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
