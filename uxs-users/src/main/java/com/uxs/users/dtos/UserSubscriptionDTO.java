package com.uxs.users.dtos;

import java.util.List;

public class UserSubscriptionDTO {

	private Long id;
	private String uuid;
	private String name;
	private String surname;
	private List<Object> subscriptions;

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public List<Object> getSubscriptions() {
		return subscriptions;
	}

	public String getSurname() {
		return surname;
	}

	public String getUuid() {
		return uuid;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSubscriptions(List<Object> subscriptions) {
		this.subscriptions = subscriptions;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
