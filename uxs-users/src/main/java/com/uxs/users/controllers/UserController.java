package com.uxs.users.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.uxs.users.dtos.UserDTO;
import com.uxs.users.dtos.UserSubscriptionDTO;
import com.uxs.users.services.UserService;

import ma.glasnost.orika.MapperFacade;

@RestController
@RequestMapping(value = "/users")
public class UserController {

	@Autowired
	private MapperFacade orika;

	@Autowired
	private UserService userService;

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Retrieves all the users registered in the DB
	 *
	 * @return a list of all users that are registered
	 */
	@GetMapping()
	public List<UserDTO> getAll() {
		return userService.getAll();
	}

	/**
	 * Retrieves the user with the id requested
	 *
	 * @param id user id we want to find
	 * @return info from the user: id, uuid, name and surname
	 */
	@GetMapping(value = "/{id}")
	public UserDTO getById(@PathVariable Long id) {
		return userService.getById(id);
	}

	/**
	 * Retrieves all the services subscribed by a user. In case of error, user owner
	 * of the id is retrieved with an empty list of services
	 *
	 * @param id user id we want to get services subscribed to
	 * @return info from the user: id, uuid, name, surname and services subscribed
	 */
	@HystrixCommand(fallbackMethod = "fallbackServices")
	@GetMapping(value = "/{id}/services/")
	public UserSubscriptionDTO getUserWithSubscriptions(@PathVariable Long id) {
		UserDTO user = userService.getById(id);
		List<Object> subscriptions = restTemplate
				.getForObject("http://uxs-subscriptions-service/subscriptions/" + user.getUuid(), List.class);
		UserSubscriptionDTO userSubscriptions = orika.map(user, UserSubscriptionDTO.class);
		userSubscriptions.setSubscriptions(subscriptions);
		return userSubscriptions;
	}

	/**
	 * Checks if an user is subscribed to the specified service. In case of error,
	 * false is returned
	 *
	 * @param id   user id
	 * @param name of the service we want to check
	 * @return true if the user is subscribed to the specified service; false in
	 *         other cases.
	 */
	@HystrixCommand(fallbackMethod = "fallbackHasServices")
	@RequestMapping(value = "/{id}/services")
	public Boolean hasUserSubscription(@PathVariable Long id, @RequestParam("name") String name) {
		UserDTO user = userService.getById(id);
		List<Object> subscriptions = restTemplate.getForObject(
				"http://uxs-subscriptions-service/subscriptions/" + user.getUuid() + "/services?name=" + name,
				List.class);
		return subscriptions != null && subscriptions.size() > 0;
	}

	/**
	 * Returns false because it's been impossible to check if an user is subscribed
	 * to a service
	 *
	 * @param id             user id
	 * @param name           service name
	 * @param hystrixCommand exception
	 * @return false
	 */
	private Boolean fallbackHasServices(Long id, String name, Throwable hystrixCommand) {
		return false;
	}

	/**
	 * Returns the user requested with an empty subscription list because it's been
	 * impossible to retrieve user's subscribed services.
	 *
	 * @param id             user id
	 * @param hystrixCommand exception
	 * @return user info with an empty subscription list
	 */
	private UserSubscriptionDTO fallbackServices(Long id, Throwable hystrixCommand) {
		UserSubscriptionDTO userSubscriptions = orika.map(userService.getById(id), UserSubscriptionDTO.class);
		userSubscriptions.setSubscriptions(new ArrayList<Object>());
		return userSubscriptions;
	}

}
