package com.uxs.users.daos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uxs.users.entities.User;

public interface UserDAO extends JpaRepository<User, Long> {

}
