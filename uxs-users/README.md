**API DOCUMENT**

This document provides required API documentation in order to know the methods provided in this API:

`/api/users`: It retrieves all the users registered. 

`/api/users/{id}`: It retrieves the user registered with the id requested.

`/api/users/{id}/services/`: It retrieves all the services the user is subscribed to.

`/api/users/{id}/services?name={nameService}`: It retrieves true or false depending on if the user is subscribed or not to the requested service. There are 3 services created:
    
- Netflix.
- HBO.
- Spotify.
    
The names of these services are case insensitive.

There are three registered users whose their ids are: 1, 2 or 3. In case of request an user who doesn't exist in the app, an empty user will be returned.


**EXAMPLES**

http://localhost:8762/api/users

http://localhost:8762/api/users/2

http://localhost:8762/api/users/1/services/

http://localhost:8762/api/users/1/services?name=netflix






*Please, in order to make requests to this endpoints, check if zuul server is running.